package com.homework.springcrud.respone;

import com.homework.springcrud.dto.CustomerDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.stream.Stream;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponeCustomer<T> {

    private LocalDateTime date;
    private Integer status;

    private String message;

    private T customer;

}

package com.homework.springcrud.respone;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeleteRespone {

    private LocalDateTime date;
    private String status;

    private String message;

}

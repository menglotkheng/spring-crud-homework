package com.homework.springcrud.services;

import com.homework.springcrud.dto.CustomerDTO;
import com.homework.springcrud.entity.Customer;

import java.util.List;

public interface CustomerServices {

    CustomerDTO insert(CustomerDTO customer);

    List<Customer> getAll();

    Customer getById(Long id);

    Customer getByName(String name);

    Customer update(Long id, CustomerDTO customer);

   String delete(Long id);


}

package com.homework.springcrud.services.impl;

import com.homework.springcrud.dto.CustomerDTO;
import com.homework.springcrud.entity.Customer;
import com.homework.springcrud.services.CustomerServices;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class CustomerServicesImpl implements CustomerServices {

    //set default value for customer
    List<Customer> customers = new ArrayList<>(List.of(new Customer(1l, "Thorng", "Male", 22, "Koh Kong"), new Customer(2l, "Menglot", "Male", 22, "Takeo")));


    //insert customer
    @Override
    public CustomerDTO insert(CustomerDTO customer) {

        Customer cs = new Customer();

        cs.setId(customers.size() + 1l);
        cs.setName(customer.getName());
        cs.setGender(customer.getGender());
        cs.setAge(customer.getAge());
        cs.setAddress(customer.getAddress());

        customers.add(cs);

        return customer;
    }


    //get all of customer
    @Override
    public List<Customer> getAll() {
        return customers;
    }

    @Override
    public Customer getById(Long id) {
        for (Customer customer : customers) {
            if (Objects.equals(customer.getId(), id)) {
                return customer;
            }
        }

        return null;
    }


    //search customer using name of customer
    @Override
    public Customer getByName(String name) {

        for (Customer customer : customers) {
            if (customer.getName().equalsIgnoreCase(name)) {
                return customer;
            }
        }

        return null;
    }

    //update customer using ID of customer
    @Override
    public Customer update(Long id, CustomerDTO customer) {

        for (Customer customer1 : customers) {
            if (Objects.equals(customer1.getId(), id)) {

                customer1.setName(customer.getName());
                customer1.setGender(customer.getGender());
                customer1.setAge(customer.getAge());
                customer1.setAddress(customer.getAddress());

                return customer1;
            }
        }

        return null;
    }


    //delete customer using ID of customer
    @Override
    public String delete(Long id) {

        for (int i = 0; i < customers.size(); i++) {
            if (id == customers.get(i).getId()) {

                customers.remove(i);
                return "Customer Delete Successfully";

            }

        }
        return "Customer Not Found !!!";
    }
}

package com.homework.springcrud.controller;


import com.homework.springcrud.dto.CustomerDTO;
import com.homework.springcrud.entity.Customer;
import com.homework.springcrud.mapper.CustomerMapper;
import com.homework.springcrud.respone.DeleteRespone;
import com.homework.springcrud.respone.ResponeCustomer;
import com.homework.springcrud.services.CustomerServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/customers")
public class CustomerController {

    @Autowired
    private CustomerServices customerServices;

    /*    insert customer
                sample request{
           http://localhost:8989/api/v1/customers/insert
            {
                "name":"Tey",
                    "gender":"Female",
                    "age":19,
                    "address":"KPC"
            }
        }*/
    @PostMapping(path = "/insert")
    public ResponseEntity<?> insert(@RequestBody CustomerDTO customer) {

        return ResponseEntity.ok(customerServices.insert(customer));
    }


    /* get all of customer
     sample request endpoint{
         http://localhost:8989/api/v1/customers/get
     }*/
    @GetMapping(path = "/get")
    public ResponseEntity<?> get() {

        return ResponseEntity.ok(new ResponeCustomer<List<Customer>>(
                LocalDateTime.now(),
                200,
                "Successfully Response Data",
                customerServices.getAll()
        ));
    }


    /*    get customer by id
        sample request endpoint{
            http://localhost:8989/api/v1/customers/get/2
        }*/
    @GetMapping(path = "/get/{id}")
    public ResponseEntity<?> getById(@PathVariable(value = "id") Long id) {

        if (customerServices.getById(id) != null) {
            return ResponseEntity.ok(new ResponeCustomer<CustomerDTO>(
                    LocalDateTime.now(),
                    200,
                    "Successfully Response Data",
                    CustomerMapper.INSTANCE.toDto(customerServices.getById(id))
            ));
        }
        return ResponseEntity.ok().body("Customer Not Found !!!");
    }

    /*    get customer by name
                sample request {
            http://localhost:8989/api/v1/customers/get?name=menglot
        }
        */
    @GetMapping(path = "/get/")
    public ResponseEntity<?> getByName(@RequestParam String name) {

        if (customerServices.getByName(name) != null) {
            return ResponseEntity.ok(new ResponeCustomer<CustomerDTO>(
                    LocalDateTime.now(),
                    200,
                    "Successfully Response Data",
                    CustomerMapper.INSTANCE.toDto(customerServices.getByName(name))
            ));
        }
        return ResponseEntity.ok().body("Customer Not Found !!!");
    }

    /*
        update customer by id
                sample request{
            http://localhost:8989/api/v1/customers/update/1

            {
                "name":"Tey",
                    "gender":"Female",
                    "age":19,
                    "address":"KPC"
            }
        }
    */
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@PathVariable(value = "id") Long id, @RequestBody CustomerDTO customer) {

        if (customerServices.update(id, customer) != null) {
            return ResponseEntity.ok(new ResponeCustomer<CustomerDTO>(
                    LocalDateTime.now(),
                    200,
                    "Successfully Update Customer ",
                    CustomerMapper.INSTANCE.toDto(customerServices.update(id, customer))
            ));
        }

        return ResponseEntity.ok().body("Customer Not Found !!!");
    }


    /*    delete by id
        sample rquest{
            http://localhost:8989/api/v1/customers/delete/2
        }*/
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable(value = "id") Long id) {


        return ResponseEntity.ok(new DeleteRespone(LocalDateTime.now(), "OK", customerServices.delete(id)));

    }

}

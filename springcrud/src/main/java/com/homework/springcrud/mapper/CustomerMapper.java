package com.homework.springcrud.mapper;


import com.homework.springcrud.dto.CustomerDTO;
import com.homework.springcrud.entity.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CustomerMapper {


    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    CustomerDTO toDto(Customer entity);

    Customer toEntity(CustomerDTO dto);

}

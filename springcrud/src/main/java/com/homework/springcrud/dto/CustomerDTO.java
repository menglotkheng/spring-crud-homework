package com.homework.springcrud.dto;

import lombok.Data;


@Data
public class CustomerDTO {

    private Long id;
    private String name;
    private String gender;
    private Integer age;
    private String address;


}
